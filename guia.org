#+TITLE: Django básico.
#+PROPERTY: header-args:shell :results output

* Crear ambiente virtual

Teniendo instalado *virtualenvwrapper* en una configuración de variables de
ambiente:

#+begin_src shell
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Trabajo
export VIRTUALENV_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
source ~/.local/bin/virtualenvwrapper.sh
export PYTHONSTARTUP=~/.pythonrc.py
#+end_src


Creamos un ambiente *proyectoweb* o como sea el nombre que escojas.

#+begin_src shell :session web :results output
mkvirtualenv proyectoweb
#+end_src

#+RESULTS:
: created virtual environment CPython3.8.10.final.0-64 in 132ms
:   creator CPython3Posix(dest=/home/david/.virtualenvs/proyectoweb, clear=False, global=False)
:   seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=/home/david/.local/share/virtualenv)
:     added seed packages: pip==21.0.1, setuptools==57.0.0, wheel==0.36.2
:   activators BashActivator,CShellActivator,FishActivator,PowerShellActivator,PythonActivator,XonshActivator

* Referencias web

Estas son importantes.

- Web oficial  :: https://www.djangoproject.com/

- Django Girls ::  https://tutorial.djangogirls.org/es/

- Django CVB :: https://ccbv.co.uk/

* Libros

- Django TWO Scoops
- Django for begginers 3.1
- Build a Website With Django 3: A complete introduction to Django 3
- Patrones de Diseño: Erich Gamme
- siul02.si.ehu.es/~alfredo/iso/06Patrones.pdf

* Sobre MVC

Django se basa principalmente en el patrón MVC

- M :: modelos
- V :: vistas
- C :: controladores

https://blog.nearsoftjobs.com/patr%C3%B3n-de-dise%C3%B1o-mvc-2366948b5fc7
https://blog.nearsoftjobs.com/patr%C3%B3n-de-dise%C3%B1o-mvc-2366948b5fc7
https://blog.nearsoftjobs.com/patr%C3%B3n-de-dise%C3%B1o-mvc-2366948b5fc7

* Instalar Django

Activamos el ambiente.

#+begin_src shell :session web 
workon proyectoweb
#+end_src

A partir de aquí podemos instalar módulos de python en ambiente.

#+begin_src shell :results output
pip install Django
#+end_src

#+RESULTS:
: Requirement already satisfied: Django in /home/david/.local/lib/python3.8/site-packages (3.2.5)
: Requirement already satisfied: asgiref<4,>=3.3.2 in /home/david/.local/lib/python3.8/site-packages (from Django) (3.4.1)
: Requirement already satisfied: pytz in /usr/lib/python3/dist-packages (from Django) (2019.3)
: Requirement already satisfied: sqlparse>=0.2.2 in /home/david/.local/lib/python3.8/site-packages (from Django) (0.4.1)

Si necesitamos actualiza pip

#+begin_src shell
pip install -U pip
#+end_src

* Crear Proyecto
:PROPERTIES:
#+header-args: shell :session web
:END:
Para crear un proyecto tenemos habilitado el comando.

#+begin_src shell :session web :results output
django-admin startproject laberinto
#+end_src

Analizamos la estructura básica del proyecto.

** Estructura

#+begin_src shell :results output
cd laberinto
ls
tree
#+end_src

#+RESULTS:
#+begin_example
laberinto
manage.py
.
├── laberinto
│   ├── asgi.py
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
└── manage.py

1 directory, 6 files
#+end_example

El *manage.py* es la puerta de entrada ya que provee diferentes comandos y
acciones.

#+begin_src shell :results output :session web
cat laberinto/manage.py
#+end_src

#+RESULTS:
#+begin_example

!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'laberinto.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
#+end_example

** Estructura
** Como usar variables de ambiente.

En sistema se pueden definir variables de ambiente.

#+name: secreto
#+begin_src shell :results output
export SECRETO="Te amo"
echo $SECRETO
#+end_src

#+RESULTS: secreto
: Te amo

Desde python se pueden rescatar estos valores con el módulo *os*.

#+begin_src python :results output 
import os
print(os.environ.get("SECRETO"))
#+end_src

Con esto, además estan los archivos para activar las variables.

#+begin_src shell :results output
cat ~/.virtualenvs/proyectoweb/bin/postactivate 
#+end_src

#+RESULTS:
: #!/bin/bash
: # This hook is sourced after this virtualenv is activated.
: 
: 
: export SECRETO="Teamo"
: export SECRET_KEY='django-insecure-tg1xk0k-k69wxq4u6b+n#jk6c8*kr@ob7tb$18ius7aghucc14'

Y para desactivarlas

#+begin_src shell :results output
cat ~/.virtualenvs/proyectoweb/bin/postdeactivate 
#+end_src

#+RESULTS:
: #!/bin/bash
: # This hook is sourced after this virtualenv is deactivated.
: 
: unset SECRETO

** Migración inicial

Para migrar el sistema base.

#+begin_src shell
./manage.py migrate
#+end_src

** Shell

Activar la shell django+python.

#+begin_src shell
./manage.py shell
#+end_src

** Acceder a db

La shell sql (sqlite) en este ccaso.

#+begin_src shell
./manage.py dbshell
#+end_src

Luego se pueden realizar consultas *SQL*.

Para listar las tablas.

#+begin_src shell
.tables
#+end_src

** Instalar las django extensions y ptpython

#+begin_src shell
pip install django-extensions
pip install ptpython
#+end_src

Las incluimos en las *INSTALLED_APPS* en *settings.py*.

#+begin_src python
INSTALLED_APPS = (
    ...
    'django_extensions',
)
#+end_src
** Crear usuario (admin)

En la terminal.

#+begin_src shell
./manage.py createsuperuser
#+end_src

** Correr servidor de desarrollo

Iniciamos el server de desarrollo con runserver.

#+begin_src shell
./manage.py runserver
#+end_src

** Settings

Es el archivo de configuración *settings.py*.

** URLS

El archivo URLS debe contener lo siguiente, la lista *urlspatterns* irá
creciendo a medida que se desarrolle la aplicación.

#+begin_src python
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    #path("laberinto/", LaberintoView.as_view())
]
#+end_src

** Admin

http://127.0.0.1:8000/admin/

** Static

* Esquema A

Creamos directorio *templates* para archivos base. Va a definir la estructura.

- creamos base template
- escogimos base css :: pure css
- definimos en settings el directorio de templates.

* Creamos app laberinto

Crear una app para mostrar *leyendas* sobre laberintos.

#+begin_src shell
./manage.py startapp leyenda
#+end_src

** Creamos urls.py

Y completamos con lo básico, declarando las urls.

#+begin_src python
from django.conf.urls import url
from django.urls import path
from .views import IndiceLeyendasView

app_name = "leyenda"


urlpatterns = [
    path("indice", IndiceLeyendasView.as_view())
]
#+end_src


Entonces, estamos /definiendo/ una nueva url a  -> leyenda/indice
que usara la *view* llamada /IndiceLeyendas.View/.


** Accedemos a views.py para creara vista.

Revisamos CVB y buscamos *TemplateView* para heredar.



* Crear app página simple


* Las urls.

Las urls son cadenas de texto que se componen en el browser para llegar a cierta información específica.

En django tenemos los archivos *urls.py* que permiten definir estas, enlazando a las urls de cada aplicación en particular.

#+name: urls_base
#+begin_src dot :file basic_urls.png ::cmdline -Kdot -Tpng -Gdpi=300
digraph Urls{
graph [dpi=300];
App_1 [label="1 - urls.py"] 
App_2 [label="2 -urls.py"]  
Principal [label="Base - urls.py"]
App_1 -> Principal
App_2 -> Principal
}
#+end_src

#+attr_latex: :width 300px
#+attr_org: :width 300
#+RESULTS: urls_base
[[file:basic_urls.png]]


También, resulta útil construir una tabla de diseño de las url.

| app_name | basic urls | name - basic | view               | url pattern | name - app | template                  | reverse check |
|----------+------------+--------------+--------------------+-------------+------------+---------------------------+---------------|
| leyenda  | leyenda    | leyenda      | IndiceLeyendasView | indice      | indice     | leyenda/index.dj.html     | x             |
|          |            |              | IcaroView          | icaro       | icaro      | leyenda/icaro.dj.html     | x             |
|          |            |              | MinotauroView      | minotauro   | minotauro  | leyenda/minotauro.dj.html | x             |
|          |            |              |                    |             |            |                           |               |
|          |            |              |                    |             |            |                           |               |
|          |            |              |                    |             |            |                           |               |
|          |            |              |                    |             |            |                           |               |
|          |            |              |                    |             |            |                           |               |
|          |            |              |                    |             |            |                           |               |
#+TBLFM: $8='(concat $3 ":" $6)

El parámetro *name* de cada path() sirve para tener una referencia del urlpattern registado.

https://docs.djangoproject.com/en/3.2/ref/urls/

Path se conforma:

#+begin_example
path(route, view, kwargs=None, name=None)
#+end_example

- route :: es el string que define la ruta
- view :: es la funcion que entrega la vista
- kwargs :: es el diccionario de opciones para la vista
- name :: es el nombre que uno le puede asignar a *route*

** Reverse

Es una función que nos permite obtener la *url* dada el nombre o name asigando al urlpatterns.

si:

- app_name :: leyenda 
- name :: icaro

#+begin_src python
from django.urls import reverse
index = reverse("leyenda:indice")
print(index)
url = reverse("leyenda:icaro")
print(url)
#+end_src

** Reverse lazy

Es como reverse pero para usos especiales.

** Resolver

Realiza la operación inverse de *reverse* y entrega un objeto con los atributos
que se relacionan con la url consultada. 

#+begin_src python
from django.urls import resolve
url_obj = resolve("/leyenda/indice")
type(url_obj)
url_obj.__dict__.keys()
url_obj
url_obj.func
#+end_src

** Enlaces en templates.

Usando el *tag* -> /url/, será posible aplicar la url reversa a un tag name
definido como enlace.

#+begin_src html
<div>
<a href="{% url 'leyenda:indice' %}">Índice</a>
</div>
#+end_src


También ponemos los enlaces a cada /leyenda/ en el índice.

#+begin_src html
<ul>
    <li>Laberinto de Ícaro - 
        <a href="{% url 'leyenda:icaro' %}">Ver</a>
    </li>
    <li>Laberinto de Minotauro - 
        <a href="{% url 'leyenda:minotauro' %}">Ver</a>
    </li>
</ul>

#+end_src



* Acceder a la lista de comandos

Muestra todos los comandos habilitado.s

#+begin_src shell
./manage.py --help
#+end_src

































