from django.contrib import admin
from .models import Leyenda
# Register your models here.


@admin.register(Leyenda)
class LeyendaAdmin(admin.ModelAdmin):
    list_display = ("id", "titulo", "fecha")
