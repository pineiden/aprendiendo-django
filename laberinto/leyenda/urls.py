from django.conf.urls import url
from django.urls import path
from .views import (IndiceLeyendasView,
                    # IcaroView,
                    # MinotauroView,
                    LeyendaView)

app_name = "leyenda"

# /leyenda/indice -> IndiceLeyendasView
urlpatterns = [
    path("indice",
         IndiceLeyendasView.as_view(),
         name="indice"),
    # path("icaro",
    #      IcaroView.as_view(),
    #      name="icaro"),
    # path("minotauro",
    #      MinotauroView.as_view(),
    #      name="minotauro"),
    path("nombre=<slug>/",
         LeyendaView.as_view(),
         name="nombre")
]
